# 项目介绍

本项目目的是指引开发者规范使用机智云平台企业API的token。

如开发者使用token不当，可能会出现安全隐患，例如：

* 频繁申请token
* 没有判断token过期时间
* 没有判断token错误码

# 核心类

* **※企业API服务类※** `com.gizwits.example.tokendemo.service.EnterpriseApiService`
    > 重点关注！！！里面有token的使用逻辑！！！
* 配置类 `com.gizwits.example.tokendemo.config.DemoParams`
    > 配置企业API对应的url、企业信息、产品信息、设备信息
* 请求控制器 `com.gizwits.example.tokendemo.controller.TestController`
    > 处理前端下发的设备控制指令


# 运行步骤

1、打开`application.yml`配置redis连接。
```
spring:
  redis:
    host: localhost
    port: 6379
    database: 0
    password:
```

2、打开`com.gizwits.example.tokendemo.config.DemoParams`配置如下信息。
```
  @Override
  public void afterPropertiesSet() throws Exception {

      eApiHost = "http://enterpriseapi.gizwits.com";
      eApiUrlToken = eApiHost + "/v1/products/{product_key}/access_token";
      eApiUrlControlDevice = eApiHost + "/v1/products/{product_key}/devices/{did}/control";

      // 企业信息应该将保存在数据库和缓存，但为了方便演示，这里直接写死
      // TODO 替换你想测试的企业id和企业secret
      enterpriseId = "xxxxxxxxxxxx";
      enterpriseSecret = "xxxxxxxxxxxx";

      // 产品信息应该保存在数据库和缓存，但为了方便演示，这里直接写死
      // TODO 替换你想测试的产品key和产品secret
      products = new LinkedList<>();
      products.add(new Product(1, "净水器", "xxxxxxxxx", "xxxxxxxxxxxx"));
      products.add(new Product(2, "空气净化器", "xxxxxxxxx", "xxxxxxxxxxxx"));

      // 设备信息应该保存在数据库，但为了方便演示，这里直接写死
      // TODO 替换你想测试的设备did
      devices = new LinkedList<>();
      devices.add(new Device(1L, "我家的净水器", "123456", "xxxxxxxxxxxx", 1));
      devices.add(new Device(2L, "别人家的净水器", "123457", "xxxxxxxxxxxx", 1));
      devices.add(new Device(3L, "我家的空气净化器", "123458", "xxxxxxxxxxxx", 2));
      devices.add(new Device(4L, "别人家的空气净化器", "123459", "xxxxxxxxxxxx", 2));
  }
```

3、启动redis服务

4、编译并运行本项目

5、向设备下发指令，使用Postman、curl等http请求工具，以`POST`方式访问[http://localhost:8080/test/control](http://localhost:8080/test/control)，
并添加请求头`Content-Type: application/json`，请求体格式如下：

数据点形式
```
{
    "deviceId":1,
    "attrs":{
        "a39Queryrecharge_Amountofmoney":1
    }
}
```
或透传形式
```
{
    "deviceId":1,
    "raw":[11,22,33]
}
```

6、查看返回内容和log检查是否请求成功，观察设备是否接收到消息，一个成功的请求返回如下：
```
{
  "errorCode": "0",
  "errorMessage": null
}
```
log打印：
```
2018-12-21 15:03:23.080  INFO 11176 --- [nio-8080-exec-1] c.g.e.t.controller.TestController        : 匹配到设备：{"did":"xxxxxxxxxxxxxxx","id":1,"mac":"123456","name":"我家的净水器","productId":1}
2018-12-21 15:03:23.081  INFO 11176 --- [nio-8080-exec-1] c.g.e.t.controller.TestController        : 匹配到产品：{"id":1,"name":"净水器","productKey":"xxxxxxxxxxxxxxx","productSecret":"xxxxxxxxxxxxxxx"}
2018-12-21 15:03:23.108 DEBUG 11176 --- [nio-8080-exec-1] c.g.example.tokendemo.http.HttpUtils     : [Http Post] url=http://enterpriseapi.gizwits.com/v1/products/xxxxxxxxxxxxxxx/access_token, headers=null, body={"enterprise_id":"xxxxxxxxxxxxxxx","enterprise_secret":"xxxxxxxxxxxxxxx","product_secret":"xxxxxxxxxxxxxxx"}
2018-12-21 15:03:24.123 DEBUG 11176 --- [nio-8080-exec-1] c.g.example.tokendemo.http.HttpUtils     : [Http Response] body={"token": "xxxxxxxxxxxxxxx", "expire_at": 1545980605}
2018-12-21 15:03:24.150 DEBUG 11176 --- [nio-8080-exec-1] c.g.example.tokendemo.http.HttpUtils     : [Http Post] url=http://enterpriseapi.gizwits.com/v1/products/xxxxxxxxxxxxxxx/devices/xxxxxxxxxxxxxxx/control, headers={"Authorization":"token xxxxxxxxxxxxxxx"}, body={"attrs":{"a39Queryrecharge_Amountofmoney":1}}
2018-12-21 15:03:24.353 DEBUG 11176 --- [nio-8080-exec-1] c.g.example.tokendemo.http.HttpUtils     : [Http Response] body={}
```