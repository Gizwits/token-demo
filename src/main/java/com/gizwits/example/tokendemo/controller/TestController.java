package com.gizwits.example.tokendemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.gizwits.example.tokendemo.entity.Device;
import com.gizwits.example.tokendemo.entity.Product;
import com.gizwits.example.tokendemo.http.HttpJsonResponse;
import com.gizwits.example.tokendemo.service.DeviceService;
import com.gizwits.example.tokendemo.service.EnterpriseApiService;
import com.gizwits.example.tokendemo.service.ProductService;
import com.gizwits.example.tokendemo.vo.WebBaseRespVO;
import com.gizwits.example.tokendemo.vo.WebDeviceControlReqVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@ControllerAdvice
@RequestMapping("/test")
public class TestController {

	private static final Logger log = LoggerFactory.getLogger(TestController.class);

	@Autowired
	private ProductService productService;
	@Autowired
	private DeviceService deviceService;
	@Autowired
	private EnterpriseApiService enterpriseApiService;

	/**
	 * 调用这个接口测试是否能控制设备，最好能测两个pk以上，才能保证token没有混用
	 */
	@PostMapping("/control")
	public WebBaseRespVO control(@RequestBody @Valid WebDeviceControlReqVO webDeviceControlReqVO) {
		Device device = deviceService.getById(webDeviceControlReqVO.getDeviceId());
		if (Objects.isNull(device)) throw new RuntimeException("设备id不正确：" + webDeviceControlReqVO.getDeviceId());
		log.info("匹配到设备：{}", JSONObject.toJSONString(device));

		Product product = productService.getById(device.getProductId());
		if (Objects.isNull(product)) throw new RuntimeException("产品id不正确：" + device.getProductId());
		log.info("匹配到产品：{}", JSONObject.toJSONString(product));


		HttpJsonResponse httpJsonResponse;
		if (Objects.nonNull(webDeviceControlReqVO.getRaw())) {
			httpJsonResponse = enterpriseApiService.controlDevice(product.getProductKey(), device.getDid(), webDeviceControlReqVO.getRaw());
		} else if (Objects.nonNull(webDeviceControlReqVO.getAttrs())) {
			httpJsonResponse = enterpriseApiService.controlDevice(product.getProductKey(), device.getDid(), webDeviceControlReqVO.getAttrs());
		} else {
			throw new RuntimeException("发送数据为空！");
		}

		if (Objects.nonNull(httpJsonResponse)) {
			WebBaseRespVO webBaseRespVO = new WebBaseRespVO();
			webBaseRespVO.setErrorCode(httpJsonResponse.isSuccess() ? "0" : "999");
			webBaseRespVO.setErrorMessage(httpJsonResponse.getErrorMessage());
			return webBaseRespVO;
		}

		throw new RuntimeException("请求企业Api返回空");
	}

	@ExceptionHandler
	public WebBaseRespVO exceptionHandler(HttpServletRequest request, HttpServletResponse httpResponse, Exception ex) {
		log.error("请求报错，url=" + request.getRequestURL(), ex);
		String errorMessage = ex.getMessage();
		if (ex instanceof MethodArgumentNotValidException) {
			Pattern pattern = Pattern.compile("default message \\[(.*?)]]; default message \\[(.*?)]]");
			Matcher matcher = pattern.matcher(errorMessage);
			StringBuilder sb = new StringBuilder();
			while (matcher.find()) {
				sb.append(matcher.group(1)).append(":").append(matcher.group(2)).append("; ");
			}
			errorMessage = sb.toString();
		}
		WebBaseRespVO webBaseRespVO = new WebBaseRespVO();
		webBaseRespVO.setErrorCode("-1");
		webBaseRespVO.setErrorMessage(errorMessage);
		return webBaseRespVO;
	}
}
