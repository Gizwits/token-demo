package com.gizwits.example.tokendemo.service;

import com.gizwits.example.tokendemo.config.DemoParams;
import com.gizwits.example.tokendemo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 应该将产品保存在数据库，但为了方便演示，这里直接写死
 */
@Service
public class ProductService {

	@Autowired
	private DemoParams demoParams;

	public Product getById(Integer id) {
		for (Product product : demoParams.getProducts()) {
			if (product.getId().equals(id)) return product;
		}
		return null;
	}

	public Product getByProductKey(String pk) {
		for (Product product : demoParams.getProducts()) {
			if (product.getProductKey().equals(pk)) return product;
		}
		return null;
	}

}
