package com.gizwits.example.tokendemo.service;

import com.alibaba.fastjson.JSONObject;
import com.gizwits.example.tokendemo.config.DemoParams;
import com.gizwits.example.tokendemo.entity.Product;
import com.gizwits.example.tokendemo.http.HttpJsonResponse;
import com.gizwits.example.tokendemo.http.HttpUtils;
import com.gizwits.example.tokendemo.vo.EApiDeviceControlReqVO;
import com.gizwits.example.tokendemo.vo.EApiTokenReqVO;
import com.gizwits.example.tokendemo.vo.EApiTokenRespVO;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class EnterpriseApiService {

	private static final Logger log = LoggerFactory.getLogger(EnterpriseApiService.class);

	@Autowired
	private RedisService redisService;
	@Autowired
	private ProductService productService;
	@Autowired
	private DemoParams demoParams;

	private final Map<String, EApiTokenRespVO> tokenMap = new HashMap<>();

	/**
	 * 先从内存拿token
	 * 如果没有拿到或者过期，再从缓存拿token
	 * 如果没有拿到或者过期，再从云平台拿token
	 * <p>
	 * 这里要求服务器时间是准确的
	 * <p>
	 * 1、如果服务器时间比机智云平台时间要小，服务器判断token为未过期，使用token请求接口时，平台判断token为已过期，
	 * 这时候通过平台返回token相关的错误码，也能触发服务器重新请求token。
	 * <p>
	 * 2、如果服务器时间比机智云平台时间要大，服务器判断token为已过期，这时候服务器会请求新的token，但平台发现token未过期，
	 * 还是返回原来的token和原来过期时间，服务器判断到返回的token还是过期的，这时候只能通过置空过期时间来兼容了
	 */
	private EApiTokenRespVO getToken(String pk) throws Exception {
		// 先从内存拿token
		EApiTokenRespVO eApiTokenRespVO = tokenMap.get(pk);
		if (Objects.nonNull(eApiTokenRespVO) && !isExpired(eApiTokenRespVO)) {
			return eApiTokenRespVO;
		}

		// 加锁，防止多个线程同时刷新token
		// TODO 如果是分布式部署，需要换成分布式锁
		synchronized (this) {
			// 如果没有拿到或者过期，再从缓存拿token
			eApiTokenRespVO = redisService.getTokenByPk(pk);
			if (Objects.nonNull(eApiTokenRespVO) && !isExpired(eApiTokenRespVO)) {
				// 将token覆盖更新到内存
				tokenMap.put(pk, eApiTokenRespVO);
				return eApiTokenRespVO;
			}

			// 如果没有拿到或者过期，再刷新token
			return refreshToken(pk);
		}
	}

	private EApiTokenRespVO refreshToken(String pk) throws Exception {
		// 从云平台拿token
		EApiTokenRespVO eApiTokenRespVO = getTokenFromCloud(pk);
		// 从云平台也拿不到，直接返回null
		if (Objects.isNull(eApiTokenRespVO)) throw new RuntimeException("token null from cloud");
		// 返回的token还是过期的，这时候只能通过置空过期时间来兼容了
		if (isExpired(eApiTokenRespVO)) {
			eApiTokenRespVO.setExpireAt(null);
		}
		// 将token覆盖更新到缓存和内存
		redisService.cacheToken(pk, eApiTokenRespVO);
		tokenMap.put(pk, eApiTokenRespVO);
		return eApiTokenRespVO;
	}

	private EApiTokenRespVO getTokenFromCloud(String pk) throws Exception {
		Product product = productService.getByProductKey(pk);
		if (Objects.isNull(product)) throw new RuntimeException("product not exist: " + pk);

		String url = demoParams.getEApiUrlToken().replace("{product_key}", product.getProductKey());

		EApiTokenReqVO eApiTokenReqVO = new EApiTokenReqVO();
		eApiTokenReqVO.setEnterpriseId(demoParams.getEnterpriseId());
		eApiTokenReqVO.setEnterpriseSecret(demoParams.getEnterpriseSecret());
		eApiTokenReqVO.setProductSecret(product.getProductSecret());

		HttpJsonResponse httpJsonResponse = HttpUtils.postJson(url, null, eApiTokenReqVO);
		if (httpJsonResponse.isSuccess()) {
			return httpJsonResponse.getJsonObject().toJavaObject(EApiTokenRespVO.class);
		}
		String errorMessage = generateErrorMessageFromCloudResponse(httpJsonResponse);
		throw new RuntimeException(errorMessage);
	}

	public HttpJsonResponse controlDevice(String pk, String did, byte[] rawByteArray) {
		String url = demoParams.getEApiUrlControlDevice().replace("{product_key}", pk).replace("{did}", did);
		int[] rawIntArray = new int[rawByteArray.length + 1];
		// 控制指令字节
		rawIntArray[0] = 0x05;
		for (int i = 0; i < rawByteArray.length; i++) {
			rawIntArray[i + 1] = rawByteArray[i] & 0xff;
		}
		EApiDeviceControlReqVO eApiDeviceControlReqVO = new EApiDeviceControlReqVO();
		eApiDeviceControlReqVO.setRaw(rawIntArray);
		return tryRequestWhitPk("POST", url, eApiDeviceControlReqVO, pk);
	}

	public HttpJsonResponse controlDevice(String pk, String did, JSONObject attrs) {
		String url = demoParams.getEApiUrlControlDevice().replace("{product_key}", pk).replace("{did}", did);
		EApiDeviceControlReqVO eApiDeviceControlReqVO = new EApiDeviceControlReqVO();
		eApiDeviceControlReqVO.setAttrs(attrs);
		return tryRequestWhitPk("POST", url, eApiDeviceControlReqVO, pk);
	}

	private HttpJsonResponse tryRequestWhitPk(String method, String url, Object body, String pk) {
		try {
			EApiTokenRespVO eApiTokenRespVO = getToken(pk);
			if (Objects.isNull(eApiTokenRespVO)) throw new RuntimeException("token null");
			HttpJsonResponse httpJsonResponse = requestWithToken(method, url, body, eApiTokenRespVO.getToken());
			// 不含有token相关的错误码，直接返回结果
			if (!hasTokenErrorCode(httpJsonResponse)) {
				return httpJsonResponse;
			}
			// 含有token相关的错误码，尝试刷新token
			// 加锁，防止多个线程同时刷新token
			// TODO 如果是分布式部署，需要换成分布式锁
			synchronized (this) {
				log.error("[EAPI] retry when token error");
				// 进来先再请求一遍，因为有可能别的线程已经刷新了token（参考单例模式）
				eApiTokenRespVO = redisService.getTokenByPk(pk);
				if (Objects.nonNull(eApiTokenRespVO) && !isExpired(eApiTokenRespVO)) {
					httpJsonResponse = requestWithToken(method, url, body, eApiTokenRespVO.getToken());
					// 不含有token相关的错误码，直接返回结果
					if (!hasTokenErrorCode(httpJsonResponse)) {
						// 将token覆盖更新到内存
						tokenMap.put(pk, eApiTokenRespVO);
						return httpJsonResponse;
					}
				}
				// 刷新token
				eApiTokenRespVO = refreshToken(pk);
				if (Objects.isNull(eApiTokenRespVO)) throw new RuntimeException("token null");
				return requestWithToken(method, url, body, eApiTokenRespVO.getToken());
			}
		} catch (Exception e) {
			log.error("[EAPI] exception", e);
			HttpJsonResponse httpJsonResponse = new HttpJsonResponse();
			httpJsonResponse.setSuccess(false);
			httpJsonResponse.setStatusCode(-1);
			httpJsonResponse.setErrorMessage(e.getMessage());
			return httpJsonResponse;
		}
	}

	private HttpJsonResponse requestWithToken(String method, String url, Object body, String token) throws HttpException {
		if (Objects.isNull(token)) throw new RuntimeException("token null");
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.AUTHORIZATION, "token " + token);
		HttpJsonResponse httpJsonResponse = null;
		if ("GET".equalsIgnoreCase(method)) {
			httpJsonResponse = HttpUtils.getJson(url, headers);
		} else if ("POST".equalsIgnoreCase(method)) {
			httpJsonResponse = HttpUtils.postJson(url, headers, body);
		}
		if (Objects.isNull(httpJsonResponse)) {
			throw new RuntimeException("response null");
		}
		if (!httpJsonResponse.isSuccess()) {
			String errorMessage = generateErrorMessageFromCloudResponse(httpJsonResponse);
			log.error(errorMessage);
			httpJsonResponse.setErrorMessage(errorMessage);
		}
		return httpJsonResponse;
	}

	/**
	 * 判断是否返回了token相关的错误码
	 * <p>
	 * 5009 token invalid
	 * 请携带token或检查token字段格式，正确格式：token ${token值}，token后面必须空一格后再写入具体的token值
	 * <p>
	 * 5010	token not match product_key
	 * 当前token和当前product_key不匹配
	 * <p>
	 * 5011	token has expired
	 * 已过期，请再次获取token
	 */
	private boolean hasTokenErrorCode(HttpJsonResponse httpJsonResponse) {
		JSONObject jsonObject = httpJsonResponse.getJsonObject();
		if (Objects.isNull(jsonObject)) return false;
		if (!jsonObject.containsKey("error_code")) return false;
		String errorCode = jsonObject.getString("error_code");
		return errorCode.equals("5009") || errorCode.equals("5010") || errorCode.equals("5011");
	}

	/**
	 * 判断token是否过期
	 */
	private boolean isExpired(EApiTokenRespVO eApiTokenRespVO) {
		return Objects.nonNull(eApiTokenRespVO.getExpireAt()) && eApiTokenRespVO.getExpireAt() < System.currentTimeMillis() / 1000;
	}

	private String generateErrorMessageFromCloudResponse(HttpJsonResponse httpJsonResponse) {
		JSONObject jsonObject = httpJsonResponse.getJsonObject();
		String errorMessage = jsonObject == null ? "response empty" : jsonObject.toJSONString();
		return "[EAPI] response error: " + errorMessage;
	}
}
