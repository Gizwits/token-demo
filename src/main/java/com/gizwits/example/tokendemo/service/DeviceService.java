package com.gizwits.example.tokendemo.service;

import com.gizwits.example.tokendemo.config.DemoParams;
import com.gizwits.example.tokendemo.entity.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {

	@Autowired
	private DemoParams demoParams;

	public Device getById(Long id) {
		for (Device device : demoParams.getDevices()) {
			if (device.getId().equals(id)) return device;
		}
		return null;
	}

}
