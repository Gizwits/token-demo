package com.gizwits.example.tokendemo.service;

import com.alibaba.fastjson.JSONObject;
import com.gizwits.example.tokendemo.vo.EApiTokenRespVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RedisService {

	private static final String KEY_EAPI_TOKEN = "eapi:token";

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	public void cacheToken(String pk, EApiTokenRespVO token) {
		redisTemplate.opsForHash().put(KEY_EAPI_TOKEN, pk, JSONObject.toJSONString(token));
	}

	public EApiTokenRespVO getTokenByPk(String pk) {
		Object o = redisTemplate.opsForHash().get(KEY_EAPI_TOKEN, pk);
		return Objects.isNull(o) ? null : JSONObject.parseObject(o.toString(), EApiTokenRespVO.class);
	}

}
