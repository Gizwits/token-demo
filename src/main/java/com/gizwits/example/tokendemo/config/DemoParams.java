package com.gizwits.example.tokendemo.config;

import com.gizwits.example.tokendemo.entity.Device;
import com.gizwits.example.tokendemo.entity.Product;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DemoParams implements InitializingBean {

	private String eApiHost;
	private String eApiUrlToken;
	private String eApiUrlControlDevice;

	private String enterpriseId;
	private String enterpriseSecret;

	private List<Product> products;

	private List<Device> devices;

	public String getEApiHost() {
		return eApiHost;
	}

	public void setEApiHost(String eApiHost) {
		this.eApiHost = eApiHost;
	}

	public String getEApiUrlToken() {
		return eApiUrlToken;
	}

	public void setEApiUrlToken(String eApiUrlToken) {
		this.eApiUrlToken = eApiUrlToken;
	}

	public String getEApiUrlControlDevice() {
		return eApiUrlControlDevice;
	}

	public void setEApiUrlControlDevice(String eApiUrlControlDevice) {
		this.eApiUrlControlDevice = eApiUrlControlDevice;
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getEnterpriseSecret() {
		return enterpriseSecret;
	}

	public void setEnterpriseSecret(String enterpriseSecret) {
		this.enterpriseSecret = enterpriseSecret;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		eApiHost = "http://enterpriseapi.gizwits.com";
		eApiUrlToken = eApiHost + "/v1/products/{product_key}/access_token";
		eApiUrlControlDevice = eApiHost + "/v1/products/{product_key}/devices/{did}/control";

		// 企业信息应该将保存在数据库和缓存，但为了方便演示，这里直接写死
		// TODO 替换你想测试的企业id和企业secret
		enterpriseId = "xxxxxxxxxxxx";
		enterpriseSecret = "xxxxxxxxxxxx";

		// 产品信息应该保存在数据库和缓存，但为了方便演示，这里直接写死
		// TODO 替换你想测试的产品key和产品secret
		products = new LinkedList<>();
		products.add(new Product(1, "净水器", "xxxxxxxxx", "xxxxxxxxxxxx"));
		products.add(new Product(2, "空气净化器", "xxxxxxxxx", "xxxxxxxxxxxx"));

		// 设备信息应该保存在数据库，但为了方便演示，这里直接写死
		// TODO 替换你想测试的设备did
		devices = new LinkedList<>();
		devices.add(new Device(1L, "我家的净水器", "123456", "xxxxxxxxxxxx", 1));
		devices.add(new Device(2L, "别人家的净水器", "123457", "xxxxxxxxxxxx", 1));
		devices.add(new Device(3L, "我家的空气净化器", "123458", "xxxxxxxxxxxx", 2));
		devices.add(new Device(4L, "别人家的空气净化器", "123459", "xxxxxxxxxxxx", 2));
	}
}
