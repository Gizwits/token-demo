package com.gizwits.example.tokendemo.vo;

import com.alibaba.fastjson.JSONObject;

public class EApiDeviceControlReqVO {

	private int[] raw;
	private JSONObject attrs;

	public int[] getRaw() {
		return raw;
	}

	public void setRaw(int[] raw) {
		this.raw = raw;
	}

	public JSONObject getAttrs() {
		return attrs;
	}

	public void setAttrs(JSONObject attrs) {
		this.attrs = attrs;
	}
}
