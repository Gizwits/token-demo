package com.gizwits.example.tokendemo.vo;

public class WebBaseRespVO {

	private String errorCode = "0";
	private String errorMessage = "success";

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
