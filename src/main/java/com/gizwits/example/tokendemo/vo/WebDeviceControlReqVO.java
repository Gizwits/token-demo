package com.gizwits.example.tokendemo.vo;

import com.alibaba.fastjson.JSONObject;

import javax.validation.constraints.NotNull;

public class WebDeviceControlReqVO {

	@NotNull
	private Long deviceId;
	private byte[] raw;
	private JSONObject attrs;

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public byte[] getRaw() {
		return raw;
	}

	public void setRaw(byte[] raw) {
		this.raw = raw;
	}

	public JSONObject getAttrs() {
		return attrs;
	}

	public void setAttrs(JSONObject attrs) {
		this.attrs = attrs;
	}
}
