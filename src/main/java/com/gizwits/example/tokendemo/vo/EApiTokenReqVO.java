package com.gizwits.example.tokendemo.vo;

import com.alibaba.fastjson.annotation.JSONField;

public class EApiTokenReqVO {
	// 企业id
	@JSONField(name = "enterprise_id")
	private String enterpriseId;
	// 企业密码
	@JSONField(name = "enterprise_secret")
	private String enterpriseSecret;
	// 产品密钥
	@JSONField(name = "product_secret")
	private String productSecret;

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getEnterpriseSecret() {
		return enterpriseSecret;
	}

	public void setEnterpriseSecret(String enterpriseSecret) {
		this.enterpriseSecret = enterpriseSecret;
	}

	public String getProductSecret() {
		return productSecret;
	}

	public void setProductSecret(String productSecret) {
		this.productSecret = productSecret;
	}
}
