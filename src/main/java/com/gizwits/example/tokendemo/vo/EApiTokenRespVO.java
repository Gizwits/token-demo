package com.gizwits.example.tokendemo.vo;

import com.alibaba.fastjson.annotation.JSONField;

public class EApiTokenRespVO {
	private String token;
	// token过期时间戳（单位秒）
	@JSONField(name = "expire_at")
	private Integer expireAt;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(Integer expireAt) {
		this.expireAt = expireAt;
	}
}
