package com.gizwits.example.tokendemo.entity;

public class Device {

	private Long id;
	private String name;
	private String mac;
	private String did;
	private Integer productId;

	public Device(Long id, String name, String mac, String did, Integer productId) {
		this.id = id;
		this.name = name;
		this.mac = mac;
		this.did = did;
		this.productId = productId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getDid() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
}
