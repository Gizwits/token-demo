package com.gizwits.example.tokendemo.entity;

public class Product {

	private Integer id;
	private String name;
	private String productKey;
	private String productSecret;

	public Product(Integer id, String name, String productKey, String productSecret) {
		this.id = id;
		this.name = name;
		this.productKey = productKey;
		this.productSecret = productSecret;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getProductSecret() {
		return productSecret;
	}

	public void setProductSecret(String productSecret) {
		this.productSecret = productSecret;
	}
}
