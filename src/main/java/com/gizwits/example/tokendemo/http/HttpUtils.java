package com.gizwits.example.tokendemo.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.Charsets;
import org.apache.http.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HttpUtils {

	private static final Logger log = LoggerFactory.getLogger(HttpUtils.class);

	public static HttpJsonResponse getJson(String url, Map<String, String> headers) throws HttpException {
		log.debug("[Http Get] url={}, headers={}", url, JSON.toJSONString(headers));
		return get(url, headers, HttpUtils::parseJsonResponse);
	}

	public static HttpJsonResponse postJson(String url, Map<String, String> headers, Object body) throws HttpException {
		String jsonString;
		if (body instanceof String) {
			jsonString = (String) body;
		} else if (body instanceof JSONObject) {
			jsonString = ((JSONObject) body).toJSONString();
		} else {
			jsonString = JSONObject.toJSONString(body);
		}
		log.debug("[Http Post] url={}, headers={}, body={}", url, JSON.toJSONString(headers), jsonString);
		if (headers == null) {
			headers = new HashMap<>();
		}
		headers.put(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());
		StringEntity stringEntity = new StringEntity(jsonString, Charsets.UTF_8);
		return post(url, headers, stringEntity, HttpUtils::parseJsonResponse);
	}

	public static <T> T get(String url, Map<String, String> headers, HttpHandler<T> httpHandler) throws HttpException {
		HttpGet httpGet = new HttpGet(url);
		if (Objects.nonNull(headers)) {
			headers.forEach(httpGet::setHeader);
		}
		return execute(httpGet, httpHandler);
	}

	public static <T> T post(String url, Map<String, String> headers, HttpEntity requestEntity, HttpHandler<T> httpHandler) throws HttpException {
		HttpPost httpPost = new HttpPost(url);
		if (Objects.nonNull(headers)) {
			headers.forEach(httpPost::setHeader);
		}
		httpPost.setEntity(requestEntity);
		return execute(httpPost, httpHandler);
	}

	private static <T> T execute(HttpUriRequest httpRequest, HttpHandler<T> httpHandler) throws HttpException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpRequest);
			return httpHandler.handle(httpResponse);
		} catch (IOException e) {
			log.error("[Http IO Error]", e);
			throw new HttpException("[Http IO Error]", e);
		} catch (Exception e) {
			log.error("[Http Unknown Error]", e);
			throw new HttpException("[Http Unknown Error]", e);
		} finally {
			HttpClientUtils.closeQuietly(httpResponse);
			HttpClientUtils.closeQuietly(httpClient);
		}
	}

	private static HttpJsonResponse parseJsonResponse(HttpResponse httpResponse) throws Exception {
		HttpJsonResponse httpJsonResponse = new HttpJsonResponse();

		int statusCode = httpResponse.getStatusLine().getStatusCode();
		httpJsonResponse.setStatusCode(statusCode);
		boolean success = String.valueOf(statusCode).matches("2\\d{2}");
		if (!success) {
			log.error("[Http Status Error] statusCode={}", statusCode);
		}
		httpJsonResponse.setSuccess(success);

		Header contentType = httpResponse.getEntity().getContentType();
		if (Objects.nonNull(contentType) && contentType.getValue().equals(ContentType.APPLICATION_JSON.getMimeType())) {
			String responseBody = EntityUtils.toString(httpResponse.getEntity(), Charsets.UTF_8);
			log.debug("[Http Response] body={}", responseBody);
			JSONObject jsonObject = JSONObject.parseObject(responseBody);
			httpJsonResponse.setJsonObject(jsonObject);
		}
		return httpJsonResponse;
	}
}
