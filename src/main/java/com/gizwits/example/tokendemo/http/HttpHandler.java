package com.gizwits.example.tokendemo.http;

import org.apache.http.HttpResponse;

public interface HttpHandler<T> {

	T handle(HttpResponse httpResponse) throws Exception;
	
}
